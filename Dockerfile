# Backend
# устанавливаем самую лёгкую версию JAVA
FROM openjdk:8-jre-alpine
# указываем ярлык. Например, разработчика образа и проч. Необязательный пункт.
LABEL maintainer="zakh.vladislav@yandex.ru"
# указание рабочей директории в контейнере
WORKDIR /app/
# копирование джарника, переименование джарника
COPY  build/libs/*.jar backend.jar
EXPOSE 8080
# команда запуска джарника
ENTRYPOINT ["java","-jar","backend.jar"] 
